// baitap1 : tính tiền lương :
// input :
var luong1Ngay = 100000;
var soNgayLam = 26;
// output:
var luong = 0;

//tổng lương:
luong = luong1Ngay * soNgayLam;
console.log("Tổng lương = ", luong);
//--------------------------------------------------------------

//baitap2:tính giá trị trung bình;

//input:
var num1 = 5;
var num2 = 6;
var num3 = 7;
var num4 = 8;
var num5 = 9;
//output:
var trungBinh = 0;

// giá trị trung bình ;
trungBinh = (num1 + num2 + num3 + num4 + num5)/5;
console.log("Giá trị trung bình = ", trungBinh);

//---------------------------------------------------------------

//baitap3 : quy đổi tiền ;

//input: 
var USD = 23500; 
var soLuongUSD = 1000;

//output:
var tongTien = 0;

//tổng tiền sau khi quy đổi sang VND

tongTien = USD * soLuongUSD;
console.log("Tổng số tiền đã quy đổi sang VND = ",tongTien);

//-----------------------------------------------------------------

//baitap4:  tính diện tích, chu vi hình chữ nhật
//input:
 var chieuDai = 22;
 var chieuRong = 44;

 //output:
 var chuVi = 0;
 var dienTich = 0;
//Chu Vi hình chữ nhật:
chuVi = (chieuDai + chieuRong) * 2;
console.log("Chu vi =",chuVi);
//diện tích hình chữ nhật:
dienTich = chieuDai * chieuRong;
console.log("Diện tích =",dienTich);
//-----------------------------------------------------------------

//baitap5: tính tổng 2 ký số:

//input:
var kySo = 64;

//output:
var hangDonVi = 0;
var hangChuc = 0;
var tong2KySo = 0;

hangDonVi = Math.floor(kySo % 10);
console.log("hàng đơn vị =",hangDonVi);
hangChuc = Math.floor(kySo / 10);
console.log("hàng chục =",hangChuc);

//Tổng 2 ký số :
tong2KySo = hangChuc + hangDonVi;
console.log("tổng 2 ký số =",tong2KySo);
//--------------------------------------------------------------------




